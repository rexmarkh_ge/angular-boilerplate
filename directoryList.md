|-- GlobalEnglish
    |-- .DS_Store
    |-- .editorconfig
    |-- .gitignore
    |-- README.md
    |-- angular.json
    |-- browserslist
    |-- package-lock.json
    |-- package.json
    |-- tsconfig.json
    |-- tslint.json
    |-- e2e
    |   |-- protractor.conf.js
    |   |-- tsconfig.e2e.json
    |   |-- src
    |       |-- app.e2e-spec.ts
    |       |-- app.po.ts
    |-- src
        |-- .DS_Store
        |-- favicon.ico
        |-- index.html
        |-- karma.conf.js
        |-- main.ts
        |-- polyfills.ts
        |-- styles.scss
        |-- test.ts
        |-- tsconfig.app.json
        |-- tsconfig.spec.json
        |-- tslint.json
        |-- app
        |   |-- .DS_Store
        |   |-- app-routing.module.ts
        |   |-- app.component.html
        |   |-- app.component.scss
        |   |-- app.component.spec.ts
        |   |-- app.component.ts
        |   |-- app.module.ts
        |   |-- core
        |   |   |-- .DS_Store
        |   |   |-- core-routing.module.ts
        |   |   |-- core.module.ts
        |   |   |-- core.service.spec.ts
        |   |   |-- core.service.ts
        |   |   |-- components
        |   |   |   |-- footer
        |   |   |   |   |-- footer.component.html
        |   |   |   |   |-- footer.component.scss
        |   |   |   |   |-- footer.component.spec.ts
        |   |   |   |   |-- footer.component.ts
        |   |   |   |-- header
        |   |   |   |   |-- header.component.html
        |   |   |   |   |-- header.component.scss
        |   |   |   |   |-- header.component.spec.ts
        |   |   |   |   |-- header.component.ts
        |   |   |   |-- login
        |   |   |       |-- login.component.html
        |   |   |       |-- login.component.scss
        |   |   |       |-- login.component.spec.ts
        |   |   |       |-- login.component.ts
        |   |   |-- guards
        |   |   |   |-- auth.guard.ts
        |   |   |-- interceptors
        |   |       |-- httpinterceptor.service.ts
        |   |-- feature
        |   |   |-- feature.module.ts
        |   |-- shared
        |       |-- .DS_Store
        |       |-- shared-routing.module.ts
        |       |-- shared.module.ts
        |       |-- shared.service.spec.ts
        |       |-- shared.service.ts
        |       |-- class
        |       |   |-- animations.ts
        |       |-- components
        |       |-- modules
        |       |   |-- material.module.ts
        |       |-- pipes
        |           |-- safe-html.pipe.ts
        |-- assets
        |   |-- .gitkeep
        |-- environments
        |   |-- environment.prod.ts
        |   |-- environment.test.ts
        |   |-- environment.ts
        |-- styles
            |-- .DS_Store
            |-- material.css
            |-- mixins.scss
            |-- theme.scss
