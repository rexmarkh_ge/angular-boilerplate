# AngularBoilerplate

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.2.

### Prerequisites
You need [node.js](http://nodejs.org) version 12 and above with [npm](http://npmjs.com) on your machine.

### Installation
This app will install all required dependencies automatically. Just run the below command in the root folder.
```SH
$ npm install
```

### Run Application and start development Server

To run this app in your browser just start everything whit the command below in the applications root folder. It will update everything and start a simple dev server on http://localhost:4200/
```SH
$ npm start
```

### Code scaffolding
To generate a new component, run the below command.
```SH
$ ng generate component component-name (or simply) ng g c component-name
```
Similarly generate directive, pipe, service, class, guard, interface, module run the below commands
```SH
$ ng generate directive|pipe|service|class|guard|interface|enum|module name
```

### Build

Run the below command to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
```SH
$ ng build
```

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


### Project Directory Structure

```SH
|-- Angular-BoilerPlate
    |-- angular.json
    |-- package.json
    |-- src
        |-- index.html
        |-- main.ts
        |-- styles.scss
        |-- app
        |   |-- app-routing.module.ts
        |   |-- app.component.html
        |   |-- app.component.scss
        |   |-- app.component.ts
        |   |-- app.module.ts
        |   |-- core
        |   |   |-- core-routing.module.ts
        |   |   |-- core.module.ts
        |   |   |-- core.service.ts
        |   |   |-- components
        |   |   |   |-- footer
        |   |   |   |-- header
        |   |   |   |-- login
        |   |   |-- guards
        |   |   |-- interceptors
        |   |-- feature
        |   |   |-- feature.module.ts
        |   |-- shared
        |       |-- shared-routing.module.ts
        |       |-- shared.module.ts
        |       |-- shared.service.ts
        |       |-- class
        |       |-- components
        |       |-- modules
        |       |-- pipes
        |-- assets
        |-- environments
        |-- styles
```