import { trigger, state, group, query, style, transition, animate } from '@angular/animations';

export const fadeInOutAnimation = trigger(
    'fadeInOutAnimation', [
      transition(':enter', [
        // style({transform: 'translateX(100%)', opacity: 0}),
        // animate('1000ms', style({transform: 'translateX(0)', opacity: 1}))
        style({opacity: 0}),
        animate('300ms', style({ opacity: 1}))
      ]),
      transition(':leave', [
        // style({transform: 'translateX(0)', opacity: 1}),
        // animate('1000ms', style({transform: 'translateX(100%)', opacity: 0}))
        style({opacity: 1}),
        animate('200ms', style({opacity: 0}))
      ])
    ]
  )

  export const fadeInAnimation = trigger(
    'fadeInAnimation', [
      transition(':enter', [
        // style({transform: 'translateX(100%)', opacity: 0}),
        // animate('1000ms', style({transform: 'translateX(0)', opacity: 1}))
        style({opacity: 0}),
        animate('300ms', style({ opacity: 1}))
      ])
    ]
  )

  export const slideInOutAnimation = trigger(
    'slideInOutAnimation', [
      transition(':enter', [
        style({transform: 'translateX(-100%)', opacity: 0}),
        animate('200ms', style({transform: 'translateX(0)', opacity: 1}))
      ]),
      transition(':leave', [
        style({transform: 'translateX(0)', opacity: 1}),
        animate('200ms', style({transform: 'translateX(-100%)', opacity: 0}))
      ])
    ]
  )

  export const downloadSlideInOutAnimation = trigger(
    'downloadSlideInOutAnimation', [
      transition(':enter', [
        style({transform: 'translateX(-100%)', opacity: 0}),
        animate('200ms', style({transform: 'translateX(0)', opacity: 1}))
      ]),
      transition(':leave', [
        style({transform: 'translateX(0)', opacity: 1, position: 'absolute',bottom : '15px'}),
        animate('200ms', style({transform: 'translateX(100%)', opacity: 0}))
      ])
    ]
  )

  export const routerTransition = trigger('routerTransition', [
    transition('* <=> *', [
      query(':enter, :leave', style({ position: 'fixed', width:'100%', opacity: 0 })
        , { optional: true }),
      group([
        query(':enter', [
          style({ opacity: 0}),
          animate('0.4s ease-in-out', style({ opacity: 1 }))
        ], { optional: true }),
        query(':leave', [
          style({  opacity: 0.3}),
          animate('0.4s ease-in-out', style({ opacity: 0 }))
        ], { optional: true }),
      ])
    ])
  ])
  
  export const toggleUpAnimation = trigger(
    'toggleUpAnimation', [
      transition(':enter', [
        style({transform: 'translateY(100%)', opacity: 0}),
        animate('200ms', style({transform: 'translateY(0)', opacity: 1}))
      ]),
      transition(':leave', [
        style({transform: 'translateY(0)', opacity: 1, position: 'absolute',bottom : '15px'}),
        animate('200ms', style({transform: 'translateY(-100%)', opacity: 0}))
      ])
    ]
  )