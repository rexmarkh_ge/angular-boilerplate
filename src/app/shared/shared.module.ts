import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { SharedRoutingModule } from './shared-routing.module';
import { MaterialModule } from './modules/material.module';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';

@NgModule({
  declarations: [
    SafeHtmlPipe
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MaterialModule,
    SharedRoutingModule
  ],
  exports: [
    MaterialModule,
    SafeHtmlPipe,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MaterialModule,
    SafeHtmlPipe
  ]
})
export class SharedModule { }
