import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

 // Loader
 private loaderSource = new BehaviorSubject(false);
 public loaderStatus = this.loaderSource.asObservable();

  constructor() { }

  changeLoaderStatus(status: boolean) {
    this.loaderSource.next(status);
  }
}
