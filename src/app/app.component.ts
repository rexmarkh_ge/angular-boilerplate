import { Component, OnInit } from '@angular/core';
import { SharedService } from './shared/shared.service';
import { fadeInOutAnimation } from './shared/class/animations';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [fadeInOutAnimation]
})
export class AppComponent implements OnInit{
  title = 'angular-boilerplate';
  showLoader: Boolean;
  constructor(private sharedService: SharedService) {

  }
  ngOnInit() {
    this.sharedService.loaderStatus.subscribe(res => {
      this.showLoader = res;
    });
  }
}
