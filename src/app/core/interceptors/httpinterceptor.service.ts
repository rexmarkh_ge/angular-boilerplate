import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
@Injectable()
export class HttpInterceptorService implements HttpInterceptor {
  constructor(
  ) { }
  // function which will be called for all http calls
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    const token = sessionStorage.getItem('token');
    let newrequest;
    newrequest = request.clone({
      setHeaders: {
       'Content-Type': 'application/json' ,
        'token': token ? token : ' '
      }
    });

    // logging the updated Parameters to browser's console
    return next.handle(newrequest).pipe(
      tap(
        event => {
          // logging the http response to browser's console in case of a success
          if (event instanceof HttpResponse) {
            // console.log('api call success :', event);
          }
        },
        error => {
          // logging the http response to browser's console in case of a failuer
          if (event instanceof HttpErrorResponse) {
            console.log('api call error :', event);
          }
          if ( (error.status === 401) || (error.status === 406) ) {
          }
        }
      )
    );
  }
}
